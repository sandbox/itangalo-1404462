<?php
/**
 * @file
 * learnsite_topics.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function learnsite_topics_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function learnsite_topics_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function learnsite_topics_flag_default_flags() {
  $flags = array();
  // Exported flag: "Proficient".
  $flags['proficient'] = array(
    'content_type' => 'node',
    'title' => 'Proficient',
    'global' => '0',
    'types' => array(
      0 => 'topic',
    ),
    'flag_short' => 'I am proficient',
    'flag_long' => 'Click here if you have mastered this topic',
    'flag_message' => 'Well done!',
    'unflag_short' => 'Remove proficiency',
    'unflag_long' => 'It is ok to forget stuff.',
    'unflag_message' => '',
    'unflag_denied_text' => 'Learned!',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'learnsite_topics',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function learnsite_topics_node_info() {
  $items = array(
    'topic' => array(
      'name' => t('Topic'),
      'base' => 'node_content',
      'description' => t('Topics are the units on the learning site. Topics can have "super topics", encompassing the given topic and more, and also topics that are require prior learning.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
