<?php
/**
 * @file
 * learnsite_pages.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function learnsite_pages_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'topics_users_proximity_topics';
  $view->description = 'A list of all the topics bordering to a user\'s existing knowledge.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Topics: user\'s proximity topics';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Topics in your learning vincinity';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['group_rendered'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Flags: proficient */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['ui_name'] = 'Topic proficiency';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Topic proficiency';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'proficient';
  /* Relationship: Prerequisite topic */
  $handler->display->display_options['relationships']['field_topic_required_target_id']['id'] = 'field_topic_required_target_id';
  $handler->display->display_options['relationships']['field_topic_required_target_id']['table'] = 'field_data_field_topic_required';
  $handler->display->display_options['relationships']['field_topic_required_target_id']['field'] = 'field_topic_required_target_id';
  $handler->display->display_options['relationships']['field_topic_required_target_id']['ui_name'] = 'Prerequisite topic';
  $handler->display->display_options['relationships']['field_topic_required_target_id']['label'] = 'Required topic';
  $handler->display->display_options['relationships']['field_topic_required_target_id']['required'] = 1;
  /* Relationship: Flags: proficient */
  $handler->display->display_options['relationships']['flag_content_rel_1']['id'] = 'flag_content_rel_1';
  $handler->display->display_options['relationships']['flag_content_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel_1']['relationship'] = 'field_topic_required_target_id';
  $handler->display->display_options['relationships']['flag_content_rel_1']['ui_name'] = 'Prerequisite proficiency';
  $handler->display->display_options['relationships']['flag_content_rel_1']['label'] = 'Prerequisite proficiency';
  $handler->display->display_options['relationships']['flag_content_rel_1']['flag'] = 'proficient';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Topic not learned */
  $handler->display->display_options['filters']['flagged']['id'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['table'] = 'flag_content';
  $handler->display->display_options['filters']['flagged']['field'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['filters']['flagged']['ui_name'] = 'Topic not learned';
  $handler->display->display_options['filters']['flagged']['value'] = '0';
  $handler->display->display_options['filters']['flagged']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['flagged']['expose']['label'] = 'Flagged';
  $handler->display->display_options['filters']['flagged']['expose']['use_operator'] = FALSE;
  $handler->display->display_options['filters']['flagged']['expose']['operator'] = 'flagged_op';
  $handler->display->display_options['filters']['flagged']['expose']['identifier'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['expose']['required'] = 1;
  $handler->display->display_options['filters']['flagged']['expose']['multiple'] = FALSE;

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Proximity topics';
  $handler->display->display_options['pane_description'] = 'A list of all the topics bordering to a user\'s existing knowledge.';
  $handler->display->display_options['pane_category']['name'] = 'Learning panes';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $export['topics_users_proximity_topics'] = $view;

  return $export;
}
