<?php
/**
 * @file
 * learnsite_curricula.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function learnsite_curricula_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function learnsite_curricula_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function learnsite_curricula_flag_default_flags() {
  $flags = array();
  // Exported flag: "Follow curriculum".
  $flags['curriculum_follow'] = array(
    'content_type' => 'node',
    'title' => 'Follow curriculum',
    'global' => '0',
    'types' => array(
      0 => 'curriculum',
    ),
    'flag_short' => 'Follow this curriculum',
    'flag_long' => 'Click here to add this curriculum to your profile',
    'flag_message' => 'GLHF!',
    'unflag_short' => 'Abandon curriculum',
    'unflag_long' => 'Click here to remove the curriculum from your list',
    'unflag_message' => 'See you next time.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'learnsite_curricula',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function learnsite_curricula_node_info() {
  $items = array(
    'curriculum' => array(
      'name' => t('Curriculum'),
      'base' => 'node_content',
      'description' => t('A curriculum is a collection of topics that should be learned.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
