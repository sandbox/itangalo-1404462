<?php
/**
 * @file
 * learnsite_tutorials.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function learnsite_tutorials_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'topic_tutorials';
  $view->description = 'A list of all tutorials in a given topic.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Topic: tutorials';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tutorials';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['group_rendered'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Static empty text */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['ui_name'] = 'Static empty text';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no tutorials for this topic. :-(';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Relationship: Flags: tutorial_viewed */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['ui_name'] = 'Tutorial status';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Tutorial status';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'tutorial_viewed';
  /* Relationship: Flags: tutorial_viewed counter */
  $handler->display->display_options['relationships']['flag_count_rel']['id'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_count_rel']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['ui_name'] = 'View count';
  $handler->display->display_options['relationships']['flag_count_rel']['label'] = 'View count';
  $handler->display->display_options['relationships']['flag_count_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel']['flag'] = 'tutorial_viewed';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flag_content';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = 'Status';
  $handler->display->display_options['fields']['ops']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['external'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['ops']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['ops']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['ops']['alter']['html'] = 0;
  $handler->display->display_options['fields']['ops']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['ops']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['ops']['hide_empty'] = 0;
  $handler->display->display_options['fields']['ops']['empty_zero'] = 0;
  $handler->display->display_options['fields']['ops']['hide_alter_empty'] = 1;
  /* Sort criterion: Popularity */
  $handler->display->display_options['sorts']['count']['id'] = 'count';
  $handler->display->display_options['sorts']['count']['table'] = 'flag_counts';
  $handler->display->display_options['sorts']['count']['field'] = 'count';
  $handler->display->display_options['sorts']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['sorts']['count']['ui_name'] = 'Popularity';
  $handler->display->display_options['sorts']['count']['order'] = 'DESC';
  /* Contextual filter: Topic */
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['id'] = 'field_tutorial_topic_target_id';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['table'] = 'field_data_field_tutorial_topic';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['field'] = 'field_tutorial_topic_target_id';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['ui_name'] = 'Topic';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['specify_validation'] = 1;
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['validate_options']['types'] = array(
    'topic' => 'topic',
  );
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['validate_options']['access'] = 0;
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_tutorial_topic_target_id']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Topic tutorials';
  $handler->display->display_options['pane_description'] = 'A list of all tutorials for a given topic.';
  $handler->display->display_options['pane_category']['name'] = 'Learning panes';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['argument_input'] = array(
    'field_tutorial_topic_target_id' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Topic',
    ),
  );
  $export['topic_tutorials'] = $view;

  return $export;
}
