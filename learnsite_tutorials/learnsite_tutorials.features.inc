<?php
/**
 * @file
 * learnsite_tutorials.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function learnsite_tutorials_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function learnsite_tutorials_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function learnsite_tutorials_flag_default_flags() {
  $flags = array();
  // Exported flag: "Tutorial status".
  $flags['tutorial_viewed'] = array(
    'content_type' => 'node',
    'title' => 'Tutorial status',
    'global' => '0',
    'types' => array(
      0 => 'tutorial',
    ),
    'flag_short' => 'Watched!',
    'flag_long' => 'Click here to mark this tutorial as watched.',
    'flag_message' => 'Way to go!',
    'unflag_short' => 'Wait – not watched!',
    'unflag_long' => '',
    'unflag_message' => 'Hm. You behave strangely.',
    'unflag_denied_text' => 'Watched.',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'learnsite_tutorials',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function learnsite_tutorials_node_info() {
  $items = array(
    'tutorial' => array(
      'name' => t('Tutorial'),
      'base' => 'node_content',
      'description' => t('A tutorial relates to a specific topic, and helps a learning to understand that topics.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
